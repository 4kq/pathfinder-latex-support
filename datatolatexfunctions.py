# import dictionary as dictionary
from fixvalues import *
from inspect import signature  # import signature to extract parameters from encounter function
from requests import *

# Global variables
columns_in_a_row: int = 0  # holds the number of alignments given when opening a tabular environment


def encounter(name: str = "Missing_Name", race: str = "", pclass: str = "", hit_die: int = 0, number: int = 1,
              cr: int = -1, hitpoints: int = 0, defenses: dict = None, melee_offenses: list = None,
              ranged_offenses: list = None, spells: list = None, before_fight: str = "", in_fight: str = "",
              morale: str = "", initiative: int = 0, page_reference: str = "No known reference.",
              strip_from_html: bool = False):
    """
    This function returns a string containing a valid LATEX syntax, which represents a box containing an encounter
    :param strip_from_html: Determines whether the values are extracted from a html, may only be true if page_reference
    :param hit_die: number of hit die the creature has
    :param pclass: contains the class of the creature
    :param race: the race of the creature
    :param name: the name of the creature
    :param number: the number of the creatures that are encountered
    :param cr: the challenge rating for one single monster of the encounter
    :param page_reference: Can contain a reference to a monster handbook or other sources/web page
    :param hitpoints: the hitpoints every of the creatures has
    :param defenses: ac, dodge chance (if any), resistance against elemental or other types of damage etc.
    :param melee_offenses: melee attacks available to the creature
    :param ranged_offenses: ranged attacks available to the creature
    :param spells: a dictionary with spells
    :param before_fight: describes behaviour or special occasions before fight
    :param in_fight: describes behaviour or special occasions in fight
    :param morale: briefly describes motivation of the creature, i.e. why does it want to fight, does anything afflict
                   it
    :param initiative: the bonus on initiative the creature gets
    :return:
    """

    if defenses is None:
        defenses = {}
    if spells is None:
        spells = {}
    if ranged_offenses is None:
        ranged_offenses = []
    if melee_offenses is None:
        melee_offenses = []
    if initiative > 0:
        initiative = "+" + str(initiative)
    elif initiative < 0:
        initiative = str(initiative)
    else:
        initiative = str(initiative)
    if hit_die == 0:
        hit_die = ""
    else:
        hit_die = str(hit_die) + " "
    if race != "":
        race += " "
    if pclass != "":
        pclass += " "
    if strip_from_html and page_reference:
        values = strip_values_from_html("")
        parameters = signature(encounter)
        # todo strip parameters from encounters and iterate over keys to find matching ones and insert values. Only
        # overwrite those where there are values given from the get request.
        for key, value in values:
            return None
    elif strip_from_html:
        print("Usage is: set strip_values_from_html to True AND give a valid html reference to strip data from.")
        return ""

    latex_code = dm()
    latex_code += tabularx()
    latex_code += hline()
    latex_code += row(textbf(name + " (" + str(number) + ")"), "Initiative: " + initiative, cr_to_str(cr),
                      cr_to_experience(cr))
    latex_code += row(multicolumn(2, "l", race + pclass, no_new_line=True), str(hit_die), page_reference, columns=3)
    latex_code += row(textbf("Lp"), hitpoints)
    latex_code += hline(1)
    if melee_offenses:
        latex_code += generate_from_list(melee_offenses)
        latex_code += hline(1)
    if ranged_offenses:
        latex_code += generate_from_list(ranged_offenses)
        latex_code += hline(1)
    if defenses:
        latex_code += defense_from_dict(defenses)

    latex_code += tabularx(begin=False)
    latex_code += dm(end=True)
    print(latex_code)
    return


def tabularx(args: list = None, begin: bool = True, width: int = 15.5):
    """
    is used to begin and to end a tabularx environment allowing an arbitrary number of columns and alignments and to
    specify its width
    :param args: defines number of columns and their alignments in order
    :param begin: binary whether the function is used to begin or to end the environment
    :param width: width interpreted in cm how width the tabular should be
    :return: latex code snippet to begin or end the tabularx
    """
    if args is None:
        args = ["l", "l", "l", "l"]
    global columns_in_a_row
    if begin:
        columns_in_a_row = len(args)
        latex_code = "\\begin{tabularx}{" + str(width) + "cm}{"
        for alignment in args:
            latex_code = latex_code + str(alignment) + " "
        latex_code = latex_code[:len(latex_code)-1]
        latex_code += "}\n"
        return latex_code
    else:
        columns_in_a_row = 0
        latex_code = "\\end{tabularx}\n"
        return latex_code


def row(*args, columns: int = 0):
    if columns_in_a_row != 0 and len(args) > 0:
        if len(args) > columns_in_a_row:
            return "%There were too many column entries given for the current row.\n"
        else:
            latex_row = ""
            for fields in args:
                latex_row += str(fields) + " & "
            latex_row = latex_row[:len(latex_row)-3]
            if len(args) < columns_in_a_row == columns or columns < len(args) < columns_in_a_row:
                for i in range(0, columns_in_a_row-len(args)):
                    latex_row += " & "
                latex_row += newline()
                # latex_row = latex_row[:len(latex_row)]
                return "%There were too few column entries given for the current row. The missing number was added " \
                       "at the end.\n" + latex_row
            elif len(args) < columns:
                for i in range(0, columns-len(args)):
                    latex_row += " & "
                latex_row += newline()
                return "%There were too few column entries given for the current row. The missing number was added at" \
                       "the end.\n" + latex_row
            latex_row += newline()
            return latex_row
    elif columns_in_a_row == 0:
        return "%There was no tabular opened before.\n"
    elif len(args) <= 0:
        return "%There were no fields given.\n"


def generate_from_list(source_list: list):
    latex_code = row(textbf("Nahkampf"), str(source_list[0][0]), str(source_list[0][1]))
    for i in range(1, len(source_list)):
        latex_code += row("", source_list[i][0], source_list[i][1])
    return latex_code


def defense_from_dict(source_dict: dict = None):
    latex_code = "Defensive"
    latex_code += hline(1)
    if "RK" in source_dict.keys():
        latex_code += textbf("RK")
    else:
        source_dict["RK"] = ""
    latex_code += " & "
    if "Berührung" in source_dict.keys():
        latex_code += "Berührung"
    else:
        source_dict["Berührung"] = ""
    latex_code += " & "
    if "Auf dem falschen Fuß" in source_dict.keys():
        latex_code += "Auf dem falschen Fuß"
    else:
        source_dict["Auf dem falschen Fuß"] = ""
    latex_code += " & " + newline()
    latex_code += row(str(source_dict["RK"]), str(source_dict["Berührung"]), str(source_dict["Auf dem falschen Fuß"]))
    latex_code += hline(1)
    if "Ref" in source_dict.keys():
        latex_code += "Ref"
    else:
        source_dict["Ref"] = ""
    latex_code += " & "
    if "Wil" in source_dict.keys():
        latex_code += "Wil"
    else:
        source_dict["Wil"] = ""
    latex_code += " & "
    if "Zäh" in source_dict.keys():
        latex_code += "Zäh"
    else:
        source_dict["Zäh"] = ""
    latex_code += " & " + newline()
    latex_code += row(str(source_dict["Ref"]), str(source_dict["Wil"]), str(source_dict["Zäh"]))
    return latex_code


def multicolumn(width: int, alignment: str = "l", text: str = "", no_new_line: bool = False):
    """
    returns a multicolumn taking up width many columns of the origins tabular, containing text within.
    :param no_new_line: opts if there is a newline at the end of the multicolumn
    :param width: number of columns used up
    :param alignment: containing alignment of the multicolumn
    :param text: text the multicolumn shall contain
    :return: string containing the latex line of the multicolumn
    """
    latex_code = "\\multicolumn{" + str(width) + "}{" + alignment + "}{" + text + "}"
    if not no_new_line:
        latex_code += newline()
    return latex_code


def hline(number: int = 2):
    latex_code = "\\\\"
    for i in range(0, number):
        latex_code += "\\hline"
    latex_code += "\\\\\n"
    return latex_code


def textbf(text: str):
    return "\\textbf{" + text + "}"


def newline(number: int = 1):
    new_line = "\\\\"
    latex_code = ""
    for i in range(0, number):
        latex_code += new_line
    return latex_code + "\n"


def cr_to_str(cr: int):
    """
    turns the cr integer into a
    :param cr:
    :return:
    """
    if cr == -1:
        cr = ""
    else:
        for i in [2, 3, 4, 6, 8]:
            if cr == 1/i:
                cr = "CR 1/" + str(i)
    if type(cr) == "int":
        cr = "CR " + str(cr)

    return cr


def cr_to_experience(cr: int):
    if cr not in experience_points.keys():
        return ""
    return str(experience_points[cr])


def dm(end: bool = False):
    if not end:
        return "\\DM{"
    else:
        return "}"


def strip_values_from_html(path: str):
    # todo write stripping function to extract values from html page prd.5footstep.
    html_string = get(path, verify=False).content
    return None
