from datatolatexfunctions import *
# from requests import *
melee_offenses = [
                    ["Durchbohren", "+4 (1W6+1)"],
]
ranged_offenses = [

                  ]
defenses = {
            "RK": 18,
            "Berührung": 12,
            "Auf dem falschen Fuß": 17,
            "Ref": 4,
            "Wil": 2,
            "Zäh": 5
}
name = "Eber"
initiative = +6
race = "Tier"
pathfinder_class = ""
hit_die = 3
page_reference = "http://prd.5footstep.de"
hitpoints = 22
encounter(name=name, initiative=initiative, race=race, pclass=pathfinder_class, hit_die=hit_die,
          page_reference=page_reference, melee_offenses=melee_offenses, ranged_offenses=ranged_offenses,
          defenses=defenses, hitpoints=hitpoints)

# print(get("http://prd.5footstep.de", verify=False).content)
